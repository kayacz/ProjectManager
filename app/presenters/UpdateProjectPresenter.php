<?php

namespace App\Presenters;

use App\Forms\FormFactory;
use App\BackendModule\Model\ProjectManager;
use Nette\Application\UI\Form;

class UpdateProjectPresenter extends BasePresenter {

    protected $projectManager;

    public function __construct(ProjectManager $projectManager) {
        parent::__construct();
        $this->projectManager = $projectManager;
    }

    public function renderDefault() {
        if (!isset($this->template->projects)) {
            $this->template->projects = $this->projectManager->getProjects();
        }
    }

    public function renderUpdate($id) {

        $project = $this->projectManager->getProjects();

        if ($project != null) {
            $this->template->project = $project;
            $this->template->id = $id;
        } else {
            echo "nic";
        }
    }

    public function hiddenFormSucceeded($form, $values) {
        $this->projectManager->deleteProject($values->hidden);
        $this->flashMessage('Projekt byl úspěšně smazán.');
        $this->redirect('UpdateProject:');
    }

    function createComponentHiddenForm() {
        $form = new Form;
        $form->getElementPrototype()->class('ajax');
        $form->addText('hidden');
        $form->addSubmit('X');
        $form->onSuccess[] = [$this, 'hiddenFormSucceeded'];
        return $form;
    }

    function createComponentUpdateName() {
        $form = new Form;
        $form->addText('id');
        $form->addText('nameOfProject');
        $form->onSuccess[] = [$this, 'updateNameFormSucceeded'];
        return $form;
    }

    function createComponentUpdateDate() {
        $form = new Form;
        $form->addText('id');
        $form->addText('date');
        $form->onSuccess[] = [$this, 'updateDateFormSucceeded'];
        return $form;
    }

    function createComponentUpdateType() {
        $form = new Form;
        $form->addText('id');
        $typeOfProject = [
            'Typ projektu' => [
                'casove omezeny' => 'Časově omezený',
                'Continuous integration' => 'Continuous integration'
            ]
        ];
  
        $form->addSelect('typeOfProject', 'Typy projektů:', $typeOfProject)
                ->setPrompt('Zvolte typ projektu');
        $form->addSubmit('uloz');
        $form->onSuccess[] = [$this, 'updateNameFormSucceeded'];
        return $form;
    }

    public function updateNameFormSucceeded($form, $values) {
        $this->projectManager->updateProject($values);
        $this->flashMessage('Projekt byl úspěšně upraven.');
    }

    public function updateDateFormSucceeded($form, $values) {
        $s = $values->date;
        $date = str_replace('/', '-', $s);
        $m = date('Y-m-d', strtotime($date));
        $values->date = $m;
        $this->projectManager->updateProject($values);
        $this->flashMessage('Projekt byl úspěšně upraven.');
    }

    public function createComponentUpdateWebProject() {

        $form = new Form;
        $form->addText('id');
        $webProject = ['webprojekt' => [
                'ano' => 'ano',
                'ne' => 'ne'
            ]
        ];

        $form->addSelect('webProject', 'Web projekt:', $webProject);
        $form->addSubmit('uloz');
        $form->onSuccess[] = [$this, 'updateNameFormSucceeded'];
        return $form;
    }

    protected function createComponentProjectForm() {
        $form = (new FormFactory())->create();
        $form->addSubmit('send', 'Odeslat')->setAttribute("class", "btn btn-default")->setAttribute('onclick', 'l()');
        $form->onSuccess[] = [$this, 'projectFormSucceeded'];
        return $form;
    }

    public function projectFormSucceeded($form, $values) {
        
        $s = $values->date;
        $m = date('Y-m-d', strtotime($s));
        $values->date=$m;
        $this->projectManager->updateProject($values);
        $this->flashMessage('Projekt byl úspěšně upraven.');
        $this->redirect('UpdateProject:default');
    }

    public function actionUpdate($id) {
        $project = $this->projectManager->getProjects();
        foreach ($project as $pr) {
            if ($pr->id == $id) {
                $this['projectForm']->setDefaults($pr->toArray());
            }
        }
    }

}
