<?php

namespace App\Presenters;

use App\Forms\FormFactory;
use App\BackendModule\Model\ProjectManager;

class HomepagePresenter extends BasePresenter {

    protected $projectManager;
    protected $userCount = 0;

    public function __construct(ProjectManager $projectManager, $userCount = 0) {
        parent::__construct();
        $this->projectManager = $projectManager;
        $this->userCount = $userCount;
    }

    protected function createComponentProjectForm() {
        $user = $this->projectManager->getUsers();
        $form = (new FormFactory())->create();
        $i = 0;
        $userCount = count($user);
        $this->userCount = $userCount;
        $users = array();
        foreach ($user as $ur) {
            $users[$ur->id] = $ur->name . " " . $ur->lastName;
        };
        for ($x = 1; $x <= $this->userCount; $x++) {
            $form->addSelect($x, 'Uživatele:', $users)->setAttribute("class", "form-control input");
        }

        $form->addSubmit('send', 'Odeslat')->setAttribute("class", "btn btn-default")->setAttribute('onclick', 'l()');
        $form->onSuccess[] = [$this, 'projectFormSucceeded'];
        return $form;
    }

    public function projectFormSucceeded($form, $values) {
        $s = $values->date;
        $m = date('Y-m-d', strtotime($s));

        $row = $this->projectManager->addProject($values->nameOfProject, $m, $values->typeOfProject, $values->webProject);
        $project = $this->projectManager->getProjects();
        $id = 0;
        foreach ($project as $pr) {
            $id = $pr->id;
        }
        $id = $id + 1;
        for ($x = 1; $x <= $this->userCount; $x++) {
            $this->projectManager->addProjectTOUser($values->$x, $id);
        }
        $this->flashMessage('Projekt byl úspěšně uložen.');
        $this->redirect('Homepage:');
    }

}
