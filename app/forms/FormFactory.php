<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;

class FormFactory {

    use Nette\SmartObject;

    /**
     * @return Form
     */
    public function create() {
        $form = new Form;
        $form->addHidden('id', null);
        $form->addText('nameOfProject', 'Název projektu')->addRule(FORM::FILLED, 'Zadej název projektu')
                ->setAttribute('placeholder', 'Název projektu')->setAttribute("class", "form-control input");
        $form->addText('date', 'Datum odevzdání')->setType('date')->addRule(FORM::FILLED, 'Zadej datum odevzdani')
                ->setAttribute('placeholder', 'Datum odevzdani')->setAttribute("class", "form-control datepicker input");
        $typeOfProject = [
            'Typ projektu' => [
                'casove omezeny' => 'Časově omezený',
                'Continuous integration' => 'Continuous integration'
            ]
        ];

        $form->addSelect('typeOfProject', 'Typy projektů:', $typeOfProject)
                ->addRule(FORM::FILLED, 'Zadej datum odevzdani')->setAttribute("class", "form-control input");
        $project = [
            'ano' => 'ano',
            'ne' => 'ne',
        ];

        $form->addRadioList('webProject', 'Webovy projekt:', $project)
                ->getSeparatorPrototype()->setName(NULL);
     
        return $form;
    }

}
