<?php

namespace App\BackendModule\Model;

use Nette;

class ProjectManager {

    use Nette\SmartObject;

    const
            TABLE_NAME = 'projects',
            COLUMN_ID = 'id',
            COLUMN_NAME = 'nameOfProject',
            COLUMN_DATE = 'date',
            COLUMN_TYPE = 'typeOfProject',
            COLUMN_ROLE = 'webProject',
            TABLE_NAME2 = 'user',
            COLUMN_ID2 = 'id',
            COLUMN_NAMEUser = 'name',
            COLUMN_LNAME = 'lastName',
            TABLE_NAME3 = 'project_user',
            ID_USER = 'id_user',
            ID_PROJECT = 'id_project'

    ;

    private $database;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    public function addProject($name, $date, $type, $webProject) {
        $l = $this->database->table(self::TABLE_NAME)->insert([
            self::COLUMN_NAME => $name,
            self::COLUMN_DATE => $date,
            self::COLUMN_TYPE => $type,
            self::COLUMN_ROLE => $webProject
                ]
        );
    }

    public function getProjects() {
        return $this->database->table(self::TABLE_NAME);
    }

    public function deleteProject($id) {
        $this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $id)->delete();
    }

    public function updateProject($values) {
        $this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $values[self::COLUMN_ID])->update($values);
    }

    public function getUsers() {
        return $this->database->table(self::TABLE_NAME2);
    }

    public function addProjectTOUser($id_user, $id_project) {
        $this->database->table(self::TABLE_NAME3)->insert([
            self::ID_PROJECT => $id_project,
            self::ID_USER => $id_user
        ]);
    }

}
