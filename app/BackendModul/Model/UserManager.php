<?php


namespace App\BackendModule\Model;
use Nette;
class UserManager {
   use Nette\SmartObject;

    const
            TABLE_NAME = 'user',
            COLUMN_ID = 'id',
            COLUMN_NAME = 'name',
            COLUMN_LNAME = 'lastName';

    private $database;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }
    public function getUsers() {
        return $this->database->table(self::TABLE_NAME);
    }
}
