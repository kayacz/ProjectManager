$(function(){
  $( ".datepicker" ).datepicker({ dateFormat: 'dd-mm-yy' });
  $('.in').hide();
  $(".update").on("click", function (event) {
      var clicked=$(this);
      clicked.hide();
      $(clicked).next().show();
      $(clicked).next().addClass("active");
});

$(".in input").on("change paste enter", function() {
     $(".val").html($(this).val());
     $(".in").hide();
     $(".update").show();
});
$(".bt").click(function(){
    $(this).data('clicked', true);
});
$(".type").on("change", function() {
     var clicked=$(this).prev().prev();
     $(clicked).html($(this).find("option:selected").attr('value') );
     if($('.bt').data('clicked')) {
        $(".type").hide();
        $(".update").show();
     }

});

});
