-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Čtv 15. čen 2017, 21:19
-- Verze serveru: 5.7.14
-- Verze PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `projectmanager`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `projects`
--

CREATE TABLE `projects` (
  `id` int(10) NOT NULL,
  `nameOfProject` varchar(80) NOT NULL,
  `date` date NOT NULL,
  `typeOfProject` varchar(80) NOT NULL,
  `webProject` varchar(80) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Vypisuji data pro tabulku `projects`
--

INSERT INTO `projects` (`id`, `nameOfProject`, `date`, `typeOfProject`, `webProject`) VALUES
(250, 'gbhhjbj56', '2017-06-06', 'casove omezeny', 'ano'),
(249, 'hbhbh44', '2017-06-14', 'casove omezeny', 'ano'),
(248, 'njnj', '2017-06-22', 'casove omezeny', 'ano'),
(247, 'fds', '2017-06-14', 'casove omezeny', 'ano'),
(246, 'fsdfds', '2017-06-08', 'casove omezeny', 'ano');

-- --------------------------------------------------------

--
-- Struktura tabulky `project_user`
--

CREATE TABLE `project_user` (
  `id` int(10) NOT NULL,
  `id_project` int(10) NOT NULL,
  `id_user` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Vypisuji data pro tabulku `project_user`
--

INSERT INTO `project_user` (`id`, `id_project`, `id_user`) VALUES
(1, 0, 1),
(2, 242, 1),
(3, 243, 1),
(4, 245, 1),
(5, 250, 1),
(6, 250, 1),
(7, 250, 1),
(8, 250, 1),
(9, 250, 1),
(10, 250, 1),
(11, 250, 1),
(12, 250, 1),
(13, 250, 1),
(14, 250, 1),
(15, 250, 1),
(16, 250, 1),
(17, 250, 1),
(18, 251, 1),
(19, 251, 1),
(20, 251, 1),
(21, 251, 1),
(22, 251, 1),
(23, 251, 1),
(24, 251, 1),
(25, 251, 1),
(26, 251, 12),
(27, 251, 9),
(28, 251, 4),
(29, 251, 10),
(30, 251, 5);

-- --------------------------------------------------------

--
-- Struktura tabulky `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `name` varchar(80) NOT NULL,
  `lastName` varchar(80) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Vypisuji data pro tabulku `user`
--

INSERT INTO `user` (`id`, `name`, `lastName`) VALUES
(1, 'Karolína', 'Balejova'),
(2, 'Michal', '?erný'),
(3, 'dalsi', 'dalsi'),
(4, 'Michal', 'Novy'),
(5, 'Petr', 'dalsi'),
(6, 'dalsi2', 'pds'),
(7, 'dalsi3', 'fds'),
(8, 'fdsf', 'dfsf'),
(9, 'dfs', 'ghf'),
(10, 'karel', 'pravy'),
(11, 'otakar', 'svoboda'),
(12, 'fds4qwe', 'rewt'),
(13, 'tewt', 'tewt');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nameOfProject` (`nameOfProject`);

--
-- Klíče pro tabulku `project_user`
--
ALTER TABLE `project_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_project` (`id_project`),
  ADD KEY `id_user` (`id_user`);

--
-- Klíče pro tabulku `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;
--
-- AUTO_INCREMENT pro tabulku `project_user`
--
ALTER TABLE `project_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pro tabulku `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
